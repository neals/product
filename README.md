# RESTAPIDocs

These examples need to be revised carefully as it shows how to work with the REST API


## Login requests.

* [submit number](submit.md) : `POST /api/submit`
* [submit sms](sms.md) : `POST /api/sms`


## Check for updates

* [get updates of number](updates.md) : `GET /api/status/{number}`


## Live URL

here's a demo link for you to test with : (will be added soon)
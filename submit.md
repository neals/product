# Submit new Number 


**URL** : `/api/submit`

**Method** : `POST`

**Data constraints**

Token is required and others are optional.

**Data example:** All fields must be sent.

```json
{
    "Number": "2343413414",
    "CountryCode": "1",
    "AgeMin": 18,
    "AgeMax": 45,
    "Distance": 50,
    "Lat": "33.63815"
    "Lon": "-86.91956"
}
```


## Success Response

**Condition** : If it success, it will send a success response

**Code** : `200 OK`

**Content example**

```
Number is Sent
```

## Error Responses

**Condition** : If there is some issue

**Code** : `401 Unauthorized`

# Submit SMS


**URL** : `/api/sms`

**Method** : `POST`

**Data constraints**

Submit SMS code and number *without country code*


**Data example:** SMS must be sent.

```json
{
    "Number": "1233413134"
    "SMS": "23451", 
}
```


## Success Response

**Condition** : If it success, it will send a success response

**Code** : `200 OK`

**Content example**

```
Checking....
```

## Error Responses

**Condition** : If there is some issue

**Code** : `500 Internal Server Error`

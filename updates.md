# Check for new messages

Request to check for new updates

**URL** : `/api/status/{number}`

**Method** : `GET`

**Data constraints**



**Url example:**

```
    /api/status/2341232413
```


## Success Response

**Condition** : Keeps track of what it does/how many new matches and other features. 

**IMPORTANT Note**: Check for it every X seconds.

**Code** : `200 OK`

**Content example**

```json
    {
        "Status": "Waiting for SMS...",
        "Number": "2341232413",
        "HadXMatches": 0,
        "NewMatches": 5,
        "Liked": 14,
        "LikesDoneForTheDay": false        
    }
```

## Error Responses

**Condition** : If number not found

**Code** : `404 NotFound`


**Content** : `[]`

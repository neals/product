# Submit number


**URL** : `/api/number`

**Method** : `POST`

**Data constraints**

Number and country code are required ( no symbols, numbers only! )

**Data example:** All fields must be sent.

```json
{
    "CountryCode": "1", 
    "Number": "615214786",
}
```


## Success Response

**Condition** : If it success, it will send a success response

**Code** : `200 OK`

**Content example**

```json
{
    "Status": "OK",
}
```

## Error Responses

**Condition** : If there is some issue

**Code** : `500 Internal Server Error`
